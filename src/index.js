const express = require('express')
// const router = new express.Router()
const userRouter = require('./controllers/user')
const app = express()
const port = process.env.port || 3000

// router.post('/test', (req, res) => {
//     console.log(req.body)
//     res.send(req.body)
// })

app.use(express.json())
app.use(userRouter)

app.listen(port, () => {
    console.log('Port is up and running in ' + port)
})