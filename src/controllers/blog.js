const express = require('express')
const router = express.Router()

router.use(express.json())

let blog = [{
    "postTitle": "", // Blog Post title
	"postContent": "", // Blog post content
	"postedDate": "", // date and time when blog post is saved
	"postedOwner": "",  // Username who posted this Blog post information.
	"tags": ["Global delivery", "near shore delivery"],
	"category": ["News"],
	"viewsCount": 0  //We will be incrementing this count when the post is loaded

}]