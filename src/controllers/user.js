const express = require('express')
const router = express.Router()
const fs = require('fs')
const path = require('path')

router.use(express.json())
const filePath = path.join(__dirname, '../Models/users.json')

// let users = [{
//     "_id": 1,
//     "userName": "Andrew",
//     "fullName": "Andrew M",	
//     "email": "Andrew@google.com",
//     "password": "asd@123",
//     "isAdmin": true,
// }, {
//     "_id": 2,
//     "userName": "Mead",
//     "fullName": "A Mead",	
//     "email": "mead@google.com",
//     "password": "qwe@123",
//     "isAdmin": false,
// }]

router.route('/user')
    .post((req, res) => {
        try {
            const dataInFile = fs.readFileSync(filePath)
            const reqBody = req.body
            let data = dataInFile.length > 0 ? JSON.parse(dataInFile) : []
            data.push(reqBody)
            fs.writeFileSync(filePath, JSON.stringify(data))
            res.send(data)
        } catch (e) {
            res.status(404).send(e)
        }
    })

    router.route('/user/:id')
    .put((req, res) => {
        const reqBody = req.body
        const reqId = req.params.id
        try {
            const dataInFile = fs.readFileSync(filePath)
            let updateData = dataInFile.length > 0 ? JSON.parse(dataInFile) : []
            const isRegisteredUser = updateData.map((user) => {
                const userEmail = user.email
                return userEmail.includes(reqBody.email)
            })
            if(!isRegisteredUser.includes(true)){
                return res.status(400).send({error: 'User email is not registered!'})
            }
            updateData = updateData.map((user) => {
                    return (parseInt(user._id) == parseInt(reqId)) ? {...user, ...reqBody} : user
            })
            fs.writeFileSync(filePath, JSON.stringify(updateData))
            res.send(updateData)
            // users = users.map((user) => {
            //     return user._id === req.body._id ? req.body : user
            // })
            // console.log(users)
            // res.send(users)
        } catch (e) {
            res.status(500).send(e)
        }
    })
    .delete((req, res) => {
        const reqBody = req.body
        const reqId = req.params.id
        try {
            const dataInFile = fs.readFileSync(filePath)
            let fileData = dataInFile.length > 0 ? JSON.parse(dataInFile) : []
            const isRegisteredUser = fileData.map((user) => {
                const userEmail = user.email
                return userEmail.includes(reqBody.email)
            })
            if(!isRegisteredUser.includes(true)){
                return res.status(400).send({error: 'User email is not registered!'})
            }
            fileData = fileData.filter((user) => 
                parseInt(user._id) !== parseInt(reqId)
            )
            fs.writeFileSync(filePath, JSON.stringify(fileData))
            // users.filter((user) => {
            //     return user._id !== req.body._id
            // })
            // console.log(users)
            res.send(fileData)
        } catch (e) {
            res.status(500).send(e)
        }
    })

module.exports = router